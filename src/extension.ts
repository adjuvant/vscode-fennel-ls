import * as child_process from "child_process"
import * as fs from "fs"
import * as vscode from "vscode"

import { Executable, LanguageClient, LanguageClientOptions, ServerOptions } from "vscode-languageclient/node"

let client: LanguageClient

export async function activate(context: vscode.ExtensionContext) {
    promptReloadOnConfigurationChange(context)

    const config = vscode.workspace.getConfiguration("fennel-lsx")
    const serverVersion = config.get("server-version")

    let executablePath

    if (serverVersion === "external") {
        executablePath = await getExternalExecutable(context)
        console.info(`[fennel-ls] Using configured executable '${executablePath}'`)
    } else if (serverVersion === "bundled") {
        executablePath = await prepareBundledExecutable(context)
        console.info(`[fennel-ls] Using bundled executable '${executablePath}'`)
    } else {
        promptToDecideServerVersion()
        return
    }

    return startLanguageServer(context, executablePath)
}

export function deactivate(): Thenable<void> | undefined {
    if (!client) {
        return undefined
    }
    return client.stop()
}

function promptToDecideServerVersion() {
    vscode.window
        .showInformationMessage(
            `Fennel-ls language server executable is not configured.
             Please decide whether you want to use the bundled version of the language server or an external one.`,
            "Open settings"
        )
        .then((answer) => {
            if (answer === "Open settings") {
                vscode.commands.executeCommand("workbench.action.openSettings", "fennel-lsx")
            }
        })
}

function getExternalExecutable(_context: vscode.ExtensionContext): Promise<string> {
    const config = vscode.workspace.getConfiguration("fennel-lsx")
    const configuredExecutable = config.get("executable")
    if (configuredExecutable && typeof configuredExecutable == "string") {
        return Promise.resolve(configuredExecutable)
    } else {
        console.error("[fennel-ls] Invalid extension path: " + configuredExecutable)
        vscode.window
            .showErrorMessage("Invalid or missing server executable path.", "Open settings")
            .then((answer) => {
                if (answer === "Open settings") {
                    vscode.commands.executeCommand("workbench.action.openSettings", "fennel-lsx.executable")
                }
            })
        return Promise.reject()
    }
}

async function prepareBundledExecutable(context: vscode.ExtensionContext): Promise<string> {
    const bundledExecutablePath = getBundledExecutablePath(context)
    if (!fs.existsSync(bundledExecutablePath)) {
        console.info(`[fennel-ls] Compiling bundled executable ${bundledExecutablePath} ...'`)
        await compileBundledExecutable(context, bundledExecutablePath)
    }
    return bundledExecutablePath
}

function getBundledExecutablePath(context: vscode.ExtensionContext): string {
    const version = context.extension.packageJSON.version
    const uri = vscode.Uri.joinPath(context.globalStorageUri, "fennel-ls." + version)
    return uri.fsPath
}

async function compileBundledExecutable(context: vscode.ExtensionContext, path: string) {
    return vscode.window
        .withProgress(
            { location: vscode.ProgressLocation.Notification, title: "Compiling fennel-ls server ..." },
            async (progress, _) => {
                // Ensure storage directory is present
                await vscode.workspace.fs.createDirectory(context.globalStorageUri)
                const output = await compile(context)
                await new Promise<void>((resolve, reject) =>
                    fs.writeFile(path, output, (err) => {
                        if (err) {
                            vscode.window.showErrorMessage("Writing output file failed: " + err)
                            reject(err)
                        }
                        resolve()
                    })
                )
                await new Promise<void>((resolve, reject) => {
                    fs.chmod(path, 0o744, (err) => {
                        if (err) {
                            vscode.window.showErrorMessage("Changing permission failed: " + err)
                            reject(err)
                        }
                        resolve()
                    })
                })
            }
        )
        .then(() => {
            vscode.window.showInformationMessage("Compilation of fennel-ls successful")
        })
}

function compile(context: vscode.ExtensionContext): Promise<string> {
    return new Promise<string>((resolve, reject) => {
        const workingDirectory = vscode.Uri.joinPath(context.extension.extensionUri, "vendor", "fennel-ls")
        child_process.execFile(
            "lua",
            ["fennel", "--require-as-include", "--compile", "src/fennel-ls.fnl"],
            {
                cwd: workingDirectory.fsPath,
                env: { ...process.env, LUA_PATH: "src/?.lua", FENNEL_PATH: "src/?.fnl" },
            },
            (error, stdout, stderr) => {
                if (error) {
                    console.error("[fennel-ls] Compilation failed: ", error, stderr)
                    vscode.window.showErrorMessage("Compilation failed: " + error)
                    reject(error)
                }
                resolve("#!/usr/bin/env lua\n" + stdout)
            }
        )
    })
}

function promptReloadOnConfigurationChange(context: vscode.ExtensionContext) {
    vscode.workspace.onDidChangeConfiguration((e) => {
        if (
            e.affectsConfiguration("fennel-lsx") ||
            e.affectsConfiguration("fennel-ls")
        ) {
            // TODO: Provide command to restart only the language server instead.
            //       And use it here when language server settings change.
            // It should never be necessary to restart the extension itself.
            // Theoretically, it sholud also not be necesarry to restart the language server
            // We are already synchronizing the fennel-ls configuration section, but the
            // current server version only loads the configuration at start, and thus, must
            // be restarted.
            vscode.window
                .showWarningMessage("Configuration change detected. Reload to apply.", "Reload window")
                .then((answer) => {
                    if (answer === "Reload window") vscode.commands.executeCommand("workbench.action.reloadWindow")
                })
        }
    }, context.subscriptions)
}

function startLanguageServer(_context: vscode.ExtensionContext, executablePath: string) {
    if (!executablePath || typeof executablePath !== "string" || executablePath.trim() === "") {
        throw new Error("Executable path not set")
    }
    const executableOptions: Executable = { command: executablePath }
    const serverOptions: ServerOptions = {
        run: executableOptions,
        debug: executableOptions,
    }

    const clientOptions: LanguageClientOptions = {
        documentSelector: spaceSeparatedConfig("language-identifiers").map((language) => {
            return { scheme: "file", language }
        }),
        initializationOptions: { "fennel-ls": vscode.workspace.getConfiguration("fennel-ls") },
        synchronize: {
            configurationSection: "fennel-ls",
        },
    }

    client = new LanguageClient("fennel-ls", "fennel-ls", serverOptions, clientOptions)

    return client.start()
}

function spaceSeparatedConfig(id: string): string[] {
    const section = vscode.workspace.getConfiguration("fennel-lsx")
    const config = section.get(id)
    if (typeof config !== "string") {
        throw new Error(`Configuration fennel-ls.${id} must be a string.`)
    }
    return config.split(" ").filter((s) => s !== "")
}
