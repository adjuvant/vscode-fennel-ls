{ pkgs ? import <nixpkgs> {} }:

let
  fennel-ls = pkgs.callPackage ./nix/fennel-ls.nix {};
  bundle-fennel-ls = pkgs.writeShellScriptBin "bundle-fennel-ls" ''
    set -eux
    test -e vendor/fennel-ls && rm -r vendor/fennel-ls
    mkdir -p vendor/fennel-ls/
    cp -r ${fennel-ls.src}/{LICENSE,*.md,src,fennel} vendor/fennel-ls/
    chmod -R +w vendor/fennel-ls/
  '';
in
pkgs.mkShell {
  buildInputs = with pkgs; [
    nodejs
    fennel-ls
    bundle-fennel-ls
  ];
}
